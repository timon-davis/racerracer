
// Project: RacerRacer 
// Created: 2014-12-06

scale# = 3

// set window properties
SetWindowTitle( "RacerRacer" )
SetWindowSize( 1024, 768, 0 )

// set display properties
SetVirtualResolution( 1024, 768 )
SetOrientationAllowed( 1, 1, 1, 1 )

// Deactivate Global Gravity
SetPhysicsGravity( 0, 0 )

// Create Background
sprBackground = CreateSprite( LoadImage( "maps/skagway-stream.png" ) )
SetSpritePosition( sprBackground, 0, 0 )
SetSpriteDepth( sprBackground, 1000 ) // Standard depth is 10
SetSpriteSize( sprBackground, 768 * scale# , 768 * scale# )


// Create Racer Sprite
sprRacer = CreateSprite( LoadImage( "racers/the-beast.png" ) )
SetSpritePosition( sprRacer, 0, 0 )
SetSpriteSize( sprRacer, 17 * scale#, 34 * scale# )
SetSpriteShape( sprRacer, 2 )
SetSpritePhysicsOn( sprRacer, 2 )
SetspritePhysicsFriction( sprRacer, 1 )
SetSpritePhysicsCanRotate( sprRacer, 1 )

TYPE Player
	sprite      // Player Sprite
	bIsMoving   // Records movement state
	speed#       // Current speed of vehicle
	maxSpeed#    // Max velocity of vehicle
	accelleration# // Accelleration Factor
	turningSpeed#  // Turning Speed
	braking#      // Braking Factor
ENDTYPE

GLOBAL ThePlayer AS Player

ThePlayer.sprite = sprRacer
ThePlayer.maxSpeed# = 250
ThePlayer.accelleration# = 4
ThePlayer.braking# = 50
ThePlayer.turningSpeed# = 2.3
SetSpritePosition( ThePlayer.sprite, 250, 250 )
ThePlayer.speed# = 0


do
	
	PlayerMovement()
	CenterCamera()

    Print( ScreenFPS() )
    Sync()
loop

FUNCTION PlayerMovement() 
	
	
	// Get current movement vectors
	vX# = GetSpritePhysicsVelocityX( ThePlayer.sprite ) 
	vY# = GetSpritePhysicsVelocityY( ThePlayer.sprite )
	
	// Get the raw power belonging to the player
	power# = ThePlayer.speed#
	
	// Get the angle of the sprite
	vRot# = GetSpriteAngle( ThePlayer.sprite )

	// If the y axis is activated, adjust the speed of the car
	IF ( NOT GetJoystickY() = 0 ) 
		
		// Increase or decrease power based on joystick input
		IF( GetJoystickY() > 0 ) THEN INC power#, ThePlayer.accelleration# * GetJoyStickY() * -1
		IF( GetJoystickY() < 0 ) THEN INC power#, ThePlayer.braking# * GetJoystickY() * -1 
		
		// Bounds checking to enforce max speed.
		IF ( power# > ThePlayer.maxSpeed# ) THEN power# = ThePlayer.maxSpeed#
		IF ( power# < 0 ) THEN power# = 0
		
		PRINT( "Power: " + STR( power# ) ) 
		
		// Update the source player object with information
		ThePlayer.speed# = power#
		
	ENDIF
	
	// If the x axis is activated, turn the vehicle
	IF ( NOT GetJoystickX() = 0 ) 
		
		// Change the sprite angle based on input
		SetSpriteAngle( ThePlayer.sprite, vRot# + ( ThePlayer.turningSpeed# * GetJoystickX() )  )
		
	ENDIF
	
	// Determine energy divide between x/y
	powerFactorX# = SIN( -1 * vRot# )
	powerFactorY# = COS( vRot# )
	powerForX# = GetSpritePhysicsVelocityX( ThePlayer.sprite )
	powerForY# = GetSpritePhysicsVelocityY( ThePlayer.sprite )
	
	PRINT ( "Real Rotation: " + STR( vRot# ) )
	PRINT( "Power X: " + STR( powerFactorX# ) )
	PRINT( "Power Y: " + STR( powerFactorY# ) ) 
	
	IF ( NOT ( GetJoystickX() = 0 AND GetJoystickY() = 0 ) )
		
		powerForX# = powerFactorX# * power#
		powerForY# = powerFactorY# * power#
		
	ENDIF
	


		

	SetSpritePhysicsVelocity( ThePlayer.sprite, powerForX#, powerForY# )

	
	
	
	
ENDFUNCTION

FUNCTION CenterCamera()
	
ENDFUNCTION
